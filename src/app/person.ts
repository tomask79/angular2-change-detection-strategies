export class Person {
    private _name: string;
    private _surname: string;
    private _active: boolean;

    constructor(name: string, surname: string, active: boolean) {
        this._name = name;
        this._surname = surname;
        this._active = active;
    }

    public getName(): string {
        return this._name;
    }

    public setName(name: string) {
        this._name = name;
    }

    public getSurname(): string {
        return this._surname;
    }

    public setSurname(surname: string) {
        this._surname = surname;
    }

    public getActive(): boolean {
        return this._active;
    }

    public setActive(active: boolean) {
        this._active = active;
    }
}