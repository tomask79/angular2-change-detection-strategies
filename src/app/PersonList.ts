import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Person } from './person';

@Component({
    selector: 'person-list',
    templateUrl: './PersonList.html',
    //changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonList {
    @Input() persons: Array<Person>;
}