import { Component, OnInit } from '@angular/core';
import { Person } from './person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular 2 change detection test';

  public persons: Array<Person> = new Array<Person>();

  public changeFirstItem() {
    this.persons[0].setActive(!this.persons[0].getActive());
  }

  public addItem() {
    // copy of persons array from beginning.
    // let newPersons = this.persons.slice(0); 
    // add new item to copy (to see that change detection started)
    this.persons.push(new Person('SomePersonsFirstName', 'SomePersonsSurname', true));
    // change the reference, to trigger change detector.
    // this.persons = newPersons;
  }

  public ngOnInit() {
    this.persons.push(new Person('Tomas', 'Kloucek', true));
    this.persons.push(new Person('Brad', 'Green', true));
    this.persons.push(new Person('Igor', 'Minar', true));
  }
}
