# Writing applications in Angular 2 [part 21] #

## Change detection strategies ##

Well, from my point of view Angular team has made change detection in Angular 2 much more easier to understand compare to previous version of Angular. Let's summarize two basic change detection strategies in Angular 2:

* **ChangeDetectionStrategy.Default**
* **ChangeDetectionStrategy.OnPush**

### Default change detection strategy ###

ChangeDetectionStrategy.Default is a strategy sometimes called **CheckAlways**. In this mode change detector is invoked on every component state change caused either by:

* Events - click, submit, …
* XHR - Fetching data from a remote server
* Timers - setTimeout(), setInterval()

Change detector then walks through every expression in the rendered components templates and compares previous value with the current value. If they're different then the associated view is updated. I highly recommend following excellent article for more detailed information: [Change Detection Explained](http://blog.thoughtram.io/angular/2016/02/22/angular-2-change-detection-explained.html). Also I recommend to read the following: [How does Angular 2 render the view after change detection](http://stackoverflow.com/questions/36581502/how-does-angular2-render-view-after-change-detetction/36581594)

### Demo of Default Change detection strategy ###

Let's create an application with parent root component holding a list of persons (child component) and two buttons changing the input list of a child component...


```
import { Component, OnInit } from '@angular/core';
import { Person } from './person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular 2 change detection test';

  public persons: Array<Person> = new Array<Person>();

  public changeFirstItem() {
    this.persons[0].setActive(!this.persons[0].getActive());
  }

  public addItem() {
    // copy of persons array from beginning.
    // let newPersons = this.persons.slice(0); 
    // add new item to copy (to see that change detection started)
    this.persons.push(new Person('SomePersonsFirstName', 'SomePersonsSurname', true));
    // change the reference, to trigger change detector.
    // this.persons = newPersons;
  }

  public ngOnInit() {
    this.persons.push(new Person('Tomas', 'Kloucek', true));
    this.persons.push(new Person('Brad', 'Green', true));
    this.persons.push(new Person('Igor', 'Minar', true));
  }
}
```
app.component.html

```
<h1> {{title}} </h1>
<person-list [persons]="persons"></person-list>
<hr />
<input type="button" value="Change first person activity" (click) = "changeFirstItem()" />
<input type="button" value="Add Person" (click) = "addItem()" />
```
Things to notice:

* Root component contains array of persons.
* Root component contains two buttons, first changes first person's activity, second adds a new item

**PersonList child component**
 
```
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Person } from './person';

@Component({
    selector: 'person-list',
    templateUrl: './PersonList.html',
    //changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonList {
    @Input() persons: Array<Person>;
}
```
**PersonList.html**

```
<ul>
    <li *ngFor="let person of persons">
        <div>Person Name: {{person.getName()}}</div>
        <div>Person Surname: {{person.getSurname()}}</div>
        <div>Person Active: {{person.getActive()}}</div>
    </li>
</ul>
```

Now if you run the application then by clicking at first button you change the activity of the first person and by clicking at "Add Person" you add new Person. 
Notice that change of the input list in the root component is seen immediately in the child component, because input Array is not shadowed into new child property.

### OnPush change detection strategy ###

If you check the **PersonList.html** then you should realize that it's a pretty much static HTML without any action component changing the person list whatsoever. Then why should Angular 2 go through every item of this list and compare old and new values during every components tree change? This costs client's CPU! Only moment when any part of a person list in the child component should be re-rendered is **when the list marked by the Input decorator changes**. So let's tell the Angular: "Hey Angular, do NOT walk through the items of person list one by one on every app change. Listen just for changes at the input list field (change of reference), if that occurs only then please trigger the change detection". This is the moment when the OnPush change strategy comes in. 

**PersonList.ts**
(uncomment the change detection strategy field)

```
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Person } from './person';

@Component({
    selector: 'person-list',
    templateUrl: './PersonList.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonList {
    @Input() persons: Array<Person>;
}
```

**app.component.ts**
(change the addItem method to be changing the persons field by setting a new reference to shallow copy of array.

```
public addItem() {
    // copy of persons array from beginning.
    let newPersons = this.persons.slice(0); 
    // add new item to copy (to see that change detection started)
    newPersons.push(new Person('SomePersonsFirstName', 'SomePersonsSurname', true));
    // change the reference, to trigger change detector.
    this.persons = newPersons;
  }
```
Now recompile and run the application again and you should see that:

* **First button stopped working**. Since OnPush strategy invokes change detector only on change of component's input field then we don't see the first person's activity change.

* Second button still works! Why? Because we changed the input field. We set new reference into it at Array containing pushed new item. That triggered the change detection, so we see the result in the DOM.

So does OnPush strategy listens only on changes at input fields? The answer is no. Stick to this line by Victor Savkin:

**When using OnPush detectors, then the framework will check an 
OnPush component when any of its input properties changes, 
when it fires an event, or when an observable fires an event
**

### How to start the demo and play with it ###

* npm install (in the directory with package.json)
* ng build
* ng serve
* hit in the browser the URL http://localhost:4200

regards

Tomas